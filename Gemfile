source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.2'

group :production do
	# for heroku deployment
	gem 'rails_12factor'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'meta_request'
end

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-form-rails'

# yt gem
gem 'youtube_it'
# CORS request handling (i.e. youtube upload)
gem 'rack-cors', :require => 'rack/cors'

# globalize for handling different translation for items
gem 'globalize', '~> 4.0.0'

# Use postgresql as the database for Active Record
gem 'pg'

# HAML templates instead of erb
gem 'haml-rails'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'
gem 'bootstrap-sass', '~> 3.1.1'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# for handling ENV variabes
gem 'figaro'

# for multiple table inheritance
gem 'acts_as_relation', '~> 1.0'

# for items tags
gem 'acts-as-taggable-on'

# for slug in URL
gem 'friendly_id', '~> 5.0.0'

#for better authorization
gem 'cancan'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.1.2'

# oauth2
gem 'omniauth-twitter'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'

# paperclip for files upload
gem "paperclip", "~> 4.1"

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
