class CreateVideos < ActiveRecord::Migration
  def up
    create_table :videos do |t|
      t.string :provider, limit: 32
      t.string :external_id, limit: 32

      t.timestamps
    end
  end
  def down
  	drop_table :videos
  end
end
