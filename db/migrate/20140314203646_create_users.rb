class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :provider, limit: 32
      t.string :uid, limit: 32
      t.string :first_name, limit: 32
      t.string :last_name, limit: 32
      t.string :displayed_name, limit: 65
      t.string :email, limit: 96
      t.string :password, limit: 64
      t.string :password_reminder_token, limit: 64
      t.string :city, limit: 32
      t.text :about_me

      t.timestamps
    end
    add_attachment :users, :avatar
  end
  def down
    drop_table :users
    remove_attachment :users, :avatar
  end
end
