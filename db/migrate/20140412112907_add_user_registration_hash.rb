class AddUserRegistrationHash < ActiveRecord::Migration
  def up
    add_column :users, :registration_hash, :string
  end
  def down
    remove_column :users, :registration_hash, :string
  end
end
