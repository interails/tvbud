class CreateItems < ActiveRecord::Migration
  def up
    create_table :items, :as_relation_superclass => true do |t|
      t.references :parent, index: true
      t.references :user, index: true
      t.boolean :active
    end
    Item.create_translation_table! :name => :string, :slug => :string, :content => :text
  end
  def down
  	drop_table :items
  	Item.drop_translation_table!
  end
end
