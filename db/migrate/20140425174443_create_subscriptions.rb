class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :subscriber, :class_name => "User"
      t.references :subscribed, :class_name => "User"
      t.timestamps
    end
  end
end
