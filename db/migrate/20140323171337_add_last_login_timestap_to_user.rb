class AddLastLoginTimestapToUser < ActiveRecord::Migration
  def up
  	add_column :users, :last_logged_at, :timestamp
  end
  def down
  	remove_column :users, :last_logged_at, :timestamp
  end
end
