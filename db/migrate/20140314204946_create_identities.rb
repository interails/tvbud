class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities do |t|
      t.string :first_name, limit: 32
      t.string :last_name, limit: 32
      t.string :email, limit: 96
      t.string :password_digest, limit: 96

      t.timestamps
    end
  end
end
