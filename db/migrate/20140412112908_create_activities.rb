class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.references :user
      t.references :item
      t.string :activity_type, limit: 32
      t.string :content, limit: 32

      t.timestamps
    end
  end
end
