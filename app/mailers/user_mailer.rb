class UserMailer < ActionMailer::Base
  default from: "no-reply@tvbud.herokuapp.com"
  
  def after_sign_up(user)
    @user = user
    @url  = 'http://tvbud.herokuapp.com'+'/auth/confirm_email'+'?token='+@user.registration_hash
    mail(to: @user.email, subject: 'TVBUD!')
  end
  
  def after_sign_up_with_provider(user, password)
    @user = user
    @password  = password
    mail(to: @user.email, subject: 'TVBUD!')
  end
  
end
