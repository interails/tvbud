module SessionsHelper
  def user_sign_in(id)
    session[:user_id] = id
  end

  def user_signed_in?
    !session[:user_id].nil?
  end

  def current_user 
    User.where(id: session[:user_id]).first
  end
  
  def user_sign_out
    session[:user_id] = nil
  end
  
end