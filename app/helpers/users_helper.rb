module UsersHelper
	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end
	
	def get_subscribed_users(user_id)
	  @subs = Subscription.where(subscriber_id: user_id)
	  @users = []
	  @subs.each do |sub|
	    @users << User.find(sub.subscribed_id)
	  end
	  @users
	end
	
	def get_subscribing_users(user_id)
	  @subs = Subscription.where(subscribed_id: user_id)
    @users = []
    @subs.each do |sub|
      @users << User.find(sub.subscriber_id)
    end
    @users
	end
	
	def get_subscribed_content(user_id)
	  @users = get_subscribing_users(user_id)
	  @items = []
	  @users.each do |user|
	    user.items.each do |item|
	      @items << item  
	    end	     
	  end
	  @items
	end
	
	def count_subscribed_users(user_id)
	  Subscription.where(subscriber_id: user_id).size
	end
	
	def count_subscribing_users(user_id)
	  puts "siemaaa"
    Subscription.where(subscribed_id: user_id).size
    
  end
	
end
