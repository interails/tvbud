module ApplicationHelper
	def get_last_logged limit=6
		User.order(:last_logged_at).reverse_order.last(limit)
	end

	def bootstrap_class_for flash_type
	    case flash_type
	      when :success
	        "alert-success" # Green
	      when :error
	        "alert-danger" # Red
	      when :alert
	        "alert-warning" # Yellow
	      when :notice
	        "alert-info" # Blue
	      else
	        flash_type.to_s
	    end
	end
	
	def default_locale
	  "pl"
	end
end
