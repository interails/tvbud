json.array!(@activities) do |activity|
  json.extract! activity, :id, :activity_type, :content
  json.url activity_url(activity, format: :json)
end
