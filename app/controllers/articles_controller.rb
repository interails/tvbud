class ArticlesController < ApplicationController
  before_action :set_article, only: [:edit, :show, :destroy]
  before_action :set_to_translate, only: [:update]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    @article.item.user = current_user
    respond_to do |format|
      if @article.save
        @item_translation = ItemTranslation.new(translation_params)
        @item_translation.item_id = @article.item.id
        @item_translation.locale = params["language"]
        @item_translation.save
        
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render action: 'show', status: :created, location: @article }
      else
        format.html { render action: 'new' }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    @item_translation = ItemTranslation.find_or_create_by_item_id_and_locale(@article.item.id, params["language"])
    @item_translation.update(translation_params)
    #render json: @item_translation
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url }
      format.json { head :no_content }
    end
  end
  
  def translate
    render json: params
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @item_translation = ItemTranslation.friendly.find(params[:id])
      @item = @item_translation.item
      puts @item_translation.locale
      @article = Article.find(@item.as_item_id)
    end
    
    def set_to_translate
      @it = ItemTranslation.where(slug: params["id"]).first
      @item = @it.item
      @article = Article.find(@item.as_item_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit()
    end
    
    def translation_params
      params.require(:article).permit(:name, :content, :slug)
    end
end
