class SessionsController < ApplicationController
  require 'digest/md5'
  def new
  end

  def create
    user = User.from_omniauth(env["omniauth.auth"])
    user.last_logged_at = Time.now

    if user.valid?   
      user.save      
      user_sign_in(user.id)
      redirect_to root_url, notice: "Signed in"
    else
      redirect_to root_url, notice: "Cannot sign in! "+user.errors.full_messages.first
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Signed out!"
  end

  def failure
    redirect_to root_url, alert: "Authentication failed, please try again."
  end
  
end
