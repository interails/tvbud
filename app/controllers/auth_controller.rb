class AuthController < ApplicationController
  require 'digest/md5'
  def register
    
  end
  
  def new
    @user = User.new(user_params)
    #render json:params
    if @user.valid?
      #@user.is_active = 'false'
      @user.save
      UserMailer.after_sign_up(@user).deliver
      redirect_to root_path, notice: "Success"
    else
      flash[:error] = @user.errors.values.join('<br>').html_safe
      render :action => 'register'
    end
  end

  def set_password
    if params[:password][:password] == params[:password][:confirmation]
      user = current_user
      user.password = Digest::MD5.hexdigest(params[:password][:password])
      user.save!
      redirect_to root_url, notice: "Signed in!"
    else
      redirect_to root_url, notice: "Password error"
    end
  end
  
  def login
    user = User.where(email: params[:email]).first
    if user && user.authenticate(params[:password])
      if user.is_active 
        user_sign_in(user.id)
        redirect_to root_path, :flash => { :success => "Signed in!" }        
      else
        redirect_to root_path, :flash => { :error => "Inactive user!" }
      end
    else 
      redirect_to auth_path, :flash => { :error => "Wrong email/password!" }
    end    
  end
  
  def confirm_email
    token = params[:token]
    users = User.where(registration_hash: token)
    if users.any?
      @user = users.first
      @user.registration_hash = nil
      @user.is_active = true
      @user.save
      redirect_to root_path, success: "Activation success"
    else
      redirect_to root_path, error: "Activation failure"
    end
  end
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
    end
end
