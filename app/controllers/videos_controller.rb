class VideosController < ApplicationController
  require 'youtube_it'
	include UsersHelper
	def index
		@videos = Video.all
	end

	def new
		@video = Video.new
  end

  def upload
    @video = Video.create
    @video.provider = params[:video['provider']]
    if @video
      @upload_info = YouTubeIt::Client.new.upload_token(params[:video],"onet.pl")
    else
      respond_to do |format|
        format.html { render "/videos/new" }
      end
    end
  end

	def create
		@video = Video.create(video_params)
		@video.user = current_user
		@video.save
		redirect_to videos_path, notice: "New video has been added!"
	end


	private
	    # Never trust parameters from the scary internet, only allow the white list through.
	    def video_params
	      params.require(:video).permit(:provider)
	    end
end