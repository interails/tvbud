class UsersController < ApplicationController
  include UsersHelper
  before_action :set_user, only: [:show, :subscribe]
  before_action :set_subscription, only: [:subscribe]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end


  def show
    @count_subscribing = count_subscribing_users(params[:id])
    @count_subscribed = count_subscribed_users(params[:id])
    @subscribing = get_subscribing_users(params[:id])
    @subscribed = get_subscribed_users(params[:id])
    @subscribed_content = get_subscribed_content(params[:id])
  end
  
  def subscribe
    process_subscription
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end
    
    def set_subscription
      @subscription = Subscription.find_or_initialize_by_subscriber_id_and_subscribed_id(current_user.id, @user.id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :provider, :uid)
    end
    
    def process_subscription
      if @subscription.new_record?
        simple_save
      else
        @subscription.destroy
        redirect_to :back
      end 
    end
  
    def simple_save
      @subscription.save
      redirect_to :back
    end
end
