class ItemsController < ApplicationController
  before_action :set_activity, only: [:like, :rate]
  
  def show
    #render json: params
    @translated_item = get_translation_for_locale_if_exists
    
    @item = @translated_item.item
    @article = Article.find(@item.as_item_id) if @item.as_item_type = "Article"
    #@comment = Comment.find(2).parent_id
    set_comments(@item)
  end
  
  def search
  	
  end
  
  def like    
    @activity.activity_type = 'like'
    process_activity
  end
  
  def rate   
    @activity.activity_type = 'rate'
    @activity.content = '5'
    process_activity
  end
  
  private
  
  def set_activity
    @activity = Activity.find_or_initialize_by_user_id_and_item_id_and_activity_type(current_user.id, params["id"], params["action"])
  end
  
  def set_comments(item)
    @comment = Comment.new  
    @comments = item.get_comments
  end
  
  def process_activity
    if @activity.activity_type == 'like'
      process_save(true)
    elsif @activity.activity_type == 'rate'
      process_save(false)
    end         
  end
  
  def process_save(destroy_if_exists)
    if @activity.new_record?
      simple_save
    elsif destroy_if_exists
      @activity.destroy
      redirect_to :back
    else
      simple_save
    end 
  end
  
  def simple_save
    @activity.save
    redirect_to :back
  end
  
  private
  
  def get_translation_for_locale_if_exists
    @translation = get_translation_for_locale(false)
    @translation = get_translation_for_locale(true) if @translation.nil?
    @translation
  end
  
  def get_translation_for_locale(default)
    @translated_item = get_item_of_translation(!params["slug"].is_i?)
    if !default
      @translation_for_locale = ItemTranslation.where(locale: params["locale"], item_id: @translated_item.id).first      
    else
      #default_locale can be found in application helper. 
      @translation_for_locale = ItemTranslation.where(locale: default_locale, item_id: @translated_item.id).first 
    end
  end
  
  def get_item_of_translation(slug_exists_in_params)
    @it = nil
    if slug_exists_in_params
      @it = ItemTranslation.where(slug: params["slug"]).first
    else
      @it = get_default_translation_of_article(params["slug"].to_i)
    end
    @item = @it.item
  end
  
  def get_default_translation_of_article(article_id)
    ItemTranslation.where(item_id: get_item_id_of_article(article_id)).first
  end
  
  def get_item_id_of_article(article_id)
    Item.where(as_item_id: article_id).first.id
  end
  
  
  
  
end