class Item < ActiveRecord::Base
  belongs_to :parent, class_name: 'Item'
  belongs_to :user
  has_many :activities
  has_many :subitems, class_name: 'Item', foreign_key: 'parent_id'
  has_many :item_translations
  translates :name, :slug, :content
  
  acts_as_ordered_taggable

  extend FriendlyId
  friendly_id :name
  
  
  def get_comments
    @item_comments = []
    @comments = Comment.all
    @comments.each do |comment|
      @item_comments << comment if comment.item.parent_id == self.id
    end    
    @item_comments
  end
  
end
