class User < ActiveRecord::Base
  include UsersHelper
  require 'digest/md5'
  has_attached_file :avatar,
                    styles: { small: '32x32#', medium: '64x64#', large: '128x128#' },
                    default_url: "/system/users/avatars/:style/missing.png"
  validates_attachment :avatar, content_type: { content_type: /\Aimage/ }
  validates_presence_of :first_name, :last_name, :email
  validates_uniqueness_of :email
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create }
  validates :password, :confirmation =>true, :presence =>true, :length => { :minimum => 5, :maximum => 40 }

  has_many :items
  has_many :activities
  

  before_save :prepare_to_save
  
  @temp_password = nil
  
  def test_h
    test_helper
  end
  
  
  def self.from_omniauth(auth)
    find_by_provider_and_uid(auth[:provider], auth[:uid]) || create_with_omniauth(auth)
  end

  def self.create_with_omniauth(auth)
    send_email = !email_exists(auth[:info][:email])
    user = create_user_from_params(auth)
    if (send_email)      
      send_email_with_password(user)
    end
    user
  end
  
  def prepare_to_save
    if self.new_record?
      crypted_password = Digest::MD5.hexdigest(self.password)
      self.password = crypted_password
      self.registration_hash = Digest::MD5.hexdigest((0...8).map { (65 + rand(26)).chr }.join)
    end
  end
  
  def authenticate(pass)
    self.password == Digest::MD5.hexdigest(pass)
  end
  
  private
  
  def self.create_user_from_params(auth) 
    create do |user|
      set_user_params(user, auth)
    end    
  end
  
  def self.set_user_params(user, auth)
    require "open-uri"
    user.provider = auth[:provider]
    user.uid = auth[:uid]
    user.first_name = auth[:info][:first_name]
    user.last_name = auth[:info][:last_name]
    user.displayed_name = user.first_name+' '+user.last_name
    user.email = auth[:info][:email]
    user.about_me = auth[:info][:description]
    user.city = auth[:info][:location].split(',').first if auth[:info][:location] != nil
    user.is_active = true
    @temp_password = user.password = rand_password
    #user.avatar = open(URI.parse(auth[:info][:image])) if auth[:info][:image] != nil
    user
  end
  
  def self.rand_password
    ('0'..'z').to_a.shuffle.first(8).join
  end
  
  def self.send_email_with_password(user)
    UserMailer.after_sign_up_with_provider(user, @temp_password).deliver
  end
  
  def self.email_exists(user_email) 
    !User.where(email: user_email).empty?    
  end
  
end