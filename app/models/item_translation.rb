class ItemTranslation < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug
  
  belongs_to :item
end
