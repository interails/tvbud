class Video < ActiveRecord::Base
	acts_as :item

  def self.yt_session
    @yt_session ||= YouTubeIt::Client.new(:username => ENV['YOUTUBE_USERNAME'], :password =>  ENV['YOUTUBE_PASSWORD'], :dev_key => ENV['YOUTUBE_DEV_KEY'])
  end

  def self.token_form(params, nexturl)
    @yt_session ||= YouTubeIt::Client.new(:username => ENV['YOUTUBE_USERNAME'], :password =>  ENV['YOUTUBE_PASSWORD'], :dev_key => ENV['YOUTUBE_DEV_KEY'])
    options = video_options(params)
    @yt_session.upload_token(options, nexturl)
  end

  private
  def self.video_options(params)
    opts = {:provider => params[:provider]}
    opts.merge(:private => "true")
  end

end
