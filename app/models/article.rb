class Article < ActiveRecord::Base
  acts_as :item
  translates :name, :content
  after_save :create_necessary_additions
  
  extend FriendlyId
  friendly_id :slug
  
  def create_necessary_additions
    #reate_translation
    create_activity
  end
  
  def create_translation  
  end
  
  def create_activity
    @activity = Activity.new
    @activity.item = self.item
    @activity.user = self.item.user
    @activity.activity_type = 'add'
    @activity.save
  end
  
end