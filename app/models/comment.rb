class Comment < ActiveRecord::Base
  acts_as :item
  after_save :create_necessary_additions
  
  
  def create_necessary_additions
    create_activity
  end
  
  def create_activity
    @activity = Activity.new
    @activity.item = self.item
    @activity.user = self.item.user
    @activity.content = self.content
    @activity.activity_type = 'comment'
    @activity.save
  end
end
