Tvbud::Application.routes.draw do

  resources :comments

  resources :activities

  root to: "main#index"

  scope "/auth" do
    match "/:provider/callback", to: "sessions#create", via: [:get, :post]
    get "", to: "sessions#new", as: 'auth'
    get "/register", to: "auth#register", as: 'register'
    post "/new", to: "auth#new"
    post "/sign_in", to: "auth#login"
    get "/failure", to: "sessions#failure"
    get "/confirm_email", to: "auth#confirm_email"
    get "/logout", to: "sessions#destroy", as: 'logout'
  end
  get 'tags/:tag', to: 'items#search', as: 'tag'
  resources :identities

  scope "/:locale" do
    resources :articles
    get '/:id/like', to: 'items#like', as: 'like_item'
    get '/:id/rate', to: 'items#rate', as: 'rate_item'
    scope :users do 
      get '/index', to: 'users#index', as: 'users'
      get '/:id', to: 'users#show', as: 'user'
      get '/:id/subscribe', to: 'users#subscribe', as: 'subscribe_user'
    end
    resources :videos do
       get 'approve', :action => 'approve'
    end
    get '/:slug', to: 'items#show', as: 'item_slugged'
  end
  match "/:locale/videos/upload", :to => 'videos#upload', via: [:post], as: 'upload_video'
end
