Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV['OAUTH_GOOGLE_ID'], ENV['OAUTH_GOOGLE_SECRET'], scope: "userinfo.email, userinfo.profile, plus.me"
  provider :facebook, ENV['OAUTH_FACEBOOK_ID'], ENV['OAUTH_FACEBOOK_SECRET'], scope: "user_location,email"
end